""" A minimalist pipelining framework for the WEASEL project. """
from .aio import AsyncContext
from .config import ConfigurationHelper
from .queue import QueueHelper

__version__ = "1.1.7"
