WEASEL Pipeline
===============

[![pipeline status](https://gitlab.com/weasel-project/weasel-pipeline/badges/master/pipeline.svg)](https://gitlab.com/weasel-project/weasel-pipeline/-/commits/master)
[![PyPI version](https://img.shields.io/pypi/v/weasel-pipeline)](https://pypi.org/project/weasel-pipeline/)
[![Documentation](https://readthedocs.org/projects/weasel-pipeline/badge/?version=latest&style=flat)](https://weasel-pipeline.readthedocs.io/en/latest/)
[![dependency status](https://img.shields.io/librariesio/release/pypi/weasel-pipeline)](https://libraries.io/pypi/weasel-pipeline)
![PyPI license](https://img.shields.io/pypi/l/weasel-pipeline)
![PyPI Python version](https://img.shields.io/pypi/pyversions/weasel-pipeline)

`weasel-pipeline` is a minimalist pipelining framework that serves as a common foundation of our scanning and data processing tool chains.

Installation instructions, examples, and further documentation available [here](https://weasel-pipeline.readthedocs.io/en/latest/).