from weasel_pipeline import QueueHelper, AsyncContext


async def pull_chars(amqp_uri):
    queue = await QueueHelper.create(amqp_uri)
    await queue.declare_queue("hello")

    async for char in queue.pull_values("hello"):
        print(char, end="", flush=True)


if __name__ == "__main__":
    AsyncContext.run(pull_chars("amqp://localhost"))
