from weasel_pipeline import ConfigurationHelper, QueueHelper, AsyncContext


async def push_numbers(config: ConfigurationHelper):
    queue = await QueueHelper.create(config.config.amqp_uri)
    await queue.declare_queue("numbers")

    for i in range(config.config.count):
        await queue.put_value("numbers", i)
        print("Pushed number: {}".format(i))


def main():
    config = ConfigurationHelper()
    config.add_argument(
        "count",
        argument_type=int,
        required=True,
        help_text="The total numer of values to push.",
    )
    config.parse()
    AsyncContext.run(push_numbers(config))


if __name__ == "__main__":
    main()
