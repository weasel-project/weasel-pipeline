import math

from weasel_pipeline import ConfigurationHelper, QueueHelper, AsyncContext


def is_prime(n: int):
    if type(n) != int:
        raise TypeError("invalid type")
    elif n < 0:
        raise ValueError("number must be >=0")
    elif n < 2:
        return False
    else:
        for i in range(2, int(math.sqrt(n)) + 1):
            if n % i == 0:
                return False
        return True


async def pull_numbers_test_prime(config: ConfigurationHelper):
    queue = await QueueHelper.create(config.config.amqp_uri)
    await queue.declare_queue("numbers")
    await queue.declare_queue("primes")
    await queue.declare_queue("notprimes")

    async for number in queue.pull_values("numbers"):
        print("Processing number: {}".format(number))
        if is_prime(number):
            await queue.put_value("primes", number)
        else:
            await queue.put_value("notprimes", number)


def main():
    config = ConfigurationHelper()
    config.parse()
    AsyncContext.run(pull_numbers_test_prime(config))


if __name__ == "__main__":
    main()
