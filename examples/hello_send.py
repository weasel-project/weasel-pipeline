from weasel_pipeline import QueueHelper, AsyncContext


async def push_chars(amqp_uri):
    queue = await QueueHelper.create(amqp_uri)
    await queue.declare_queue("hello")

    hello = "Hello world! Each single character of this string is transmitted through our pipeline."
    for char in hello:
        await queue.put_value("hello", char)
        print("Pushed char: {}".format(char))


if __name__ == "__main__":
    AsyncContext.run(push_chars("amqp://localhost"))
