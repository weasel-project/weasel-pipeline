from weasel_pipeline import ConfigurationHelper, QueueHelper, AsyncContext


async def pull_numbers(config: ConfigurationHelper):
    queue = await QueueHelper.create(config.config.amqp_uri)
    await queue.declare_queue(config.config.queue_name)

    async for number in queue.pull_values(config.config.queue_name):
        print("Got number: {}".format(number))


def main():
    config = ConfigurationHelper()
    config.add_argument(
        "queue_name",
        argument_type=str,
        required=True,
        help_text="The name of the queue from where numbers are pulled.",
    )
    config.parse()
    AsyncContext.run(pull_numbers(config))


if __name__ == "__main__":
    main()
