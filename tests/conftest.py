import aio_pika
import asyncio
import pytest

from weasel_pipeline import QueueHelper

def pytest_addoption(parser):
    _help_amqp_uri = 'Tests which require a RabbitMQ server will connect to this amqp URI.'
    parser.addini(
        name="amqp_uri",
        help=_help_amqp_uri,
        default='amqp://localhost',
    )
    parser.addoption(
        "--amqp-uri", action="store", dest="amqp_uri", help=_help_amqp_uri
    )

async def teardown_rabbitmq(queue_helper):
    for queue in queue_helper._queues.values():
        await queue.delete(if_empty=False, if_unused=False)
    await queue_helper.close()

@pytest.fixture(scope="function")
async def queue_helper_fixture(request):
    amqp_uri = request.config.getoption('amqp_uri')
    queue_helper = await QueueHelper.create(amqp_uri)
    yield queue_helper
    await teardown_rabbitmq(queue_helper)
