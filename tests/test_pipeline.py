import asyncio
import pytest

from weasel_pipeline import QueueHelper

class TestQueue:
    @pytest.mark.asyncio
    async def test_connect_nonexistent_raises_connection_error(self):
        with pytest.raises(ConnectionError):
            await QueueHelper.create('amqp://someserverthatdoesnotexist.example.org')

    @pytest.mark.asyncio
    async def test_declare_queue(self, queue_helper_fixture):
        await queue_helper_fixture.declare_queue('test')

    @pytest.mark.asyncio
    async def test_push_pull(self, queue_helper_fixture):
        item = 'just testing'
        await queue_helper_fixture.declare_queue('test')
        await queue_helper_fixture.put_value('test', item)
        async for queue_item in queue_helper_fixture.pull_values('test'):
            assert queue_item == item
            break

    @pytest.mark.asyncio
    async def test_queue_max_length(self, queue_helper_fixture):
        item = 'just testing'
        await queue_helper_fixture.declare_queue('test', queue_max_length=1)
        await queue_helper_fixture.put_value('test', item)
        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(queue_helper_fixture.put_value('test', item), timeout=1.0)
