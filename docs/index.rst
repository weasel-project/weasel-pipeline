WEASEL Pipeline
===============

A minimalist pipelining framework for the WEASEL project.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

	Installation <installation>
	Quickstart <quickstart>
	Examples <examples>
	API Documentation <api>

