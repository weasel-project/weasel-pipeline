Quickstart
==============

Here are some quick examples to get you started after :ref:`installing WEASEL pipeline <installation>`. For a more detailed explanation, please refer to the :ref:`examples page <examples>`.


hello_send.py
-------------

.. code-block:: python

	from weasel_pipeline import QueueHelper, AsyncContext


	async def push_chars(amqp_uri):
		queue = await QueueHelper.create(amqp_uri)
		await queue.declare_queue("hello")

		hello = "Hello world! Each single character of this string is transmitted through our pipeline."
		for char in hello:
			await queue.put_value("hello", char)
			print("Pushed char: {}".format(char))


	if __name__ == "__main__":
		AsyncContext.run(push_chars("amqp://localhost"))


hello_receive.py
----------------

.. code-block:: python

	from weasel_pipeline import QueueHelper, AsyncContext


	async def pull_chars(amqp_uri):
		queue = await QueueHelper.create(amqp_uri)
		await queue.declare_queue("hello")

		async for char in queue.pull_values("hello"):
			print(char, end="", flush=True)


	if __name__ == "__main__":
		AsyncContext.run(pull_chars("amqp://localhost"))

Running the example
-------------------

.. code-block:: bash

	> python hello_send.py
	Pushed char: H
	Pushed char: e
	Pushed char: l
	Pushed char: l
	Pushed char: o
	Pushed char:  
	Pushed char: w
	Pushed char: o
	Pushed char: r
	Pushed char: l
	Pushed char: d
	Pushed char: !
	Pushed char:  
	Pushed char: E
	Pushed char: a
	Pushed char: c
	Pushed char: h
	Pushed char:  
	Pushed char: s
	Pushed char: i
	Pushed char: n
	Pushed char: g
	Pushed char: l
	Pushed char: e
	Pushed char:  
	Pushed char: c
	Pushed char: h
	Pushed char: a
	Pushed char: r
	Pushed char: a
	Pushed char: c
	Pushed char: t
	Pushed char: e
	Pushed char: r
	Pushed char:  
	Pushed char: o
	Pushed char: f
	Pushed char:  
	Pushed char: t
	Pushed char: h
	Pushed char: i
	Pushed char: s
	Pushed char:  
	Pushed char: s
	Pushed char: t
	Pushed char: r
	Pushed char: i
	Pushed char: n
	Pushed char: g
	Pushed char:  
	Pushed char: i
	Pushed char: s
	Pushed char:  
	Pushed char: t
	Pushed char: r
	Pushed char: a
	Pushed char: n
	Pushed char: s
	Pushed char: m
	Pushed char: i
	Pushed char: t
	Pushed char: t
	Pushed char: e
	Pushed char: d
	Pushed char:  
	Pushed char: t
	Pushed char: h
	Pushed char: r
	Pushed char: o
	Pushed char: u
	Pushed char: g
	Pushed char: h
	Pushed char:  
	Pushed char: o
	Pushed char: u
	Pushed char: r
	Pushed char:  
	Pushed char: p
	Pushed char: i
	Pushed char: p
	Pushed char: e
	Pushed char: l
	Pushed char: i
	Pushed char: n
	Pushed char: e
	Pushed char: .

.. code-block:: bash

	> python hello_receive.py
	Hello world! Each single character of this string is transmitted through our pipeline.